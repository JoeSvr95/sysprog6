#include "mask.h"
#include <stdio.h>
static int mask_owner, mask_group, mask_other;

int main() {
	char answr;
	char permiso;

	printf("Permisos para owner\n");
	permiso = 'r';
	printf("Permiso lectura para owner?[s/n]: ");
	scanf("%c", &answr);
	setPermiso(&mask_owner,permiso,(answr=='s') ? SET : UNSET);
	
	permiso = 'w';
	printf("Permiso escritura para owner?[s/n]: ");
	scanf(" %c", &answr);
	setPermiso(&mask_owner,permiso,(answr=='s') ? SET : UNSET);

	permiso = 'x';
	printf("Permiso ejecucion para owner?[s/n]: ");
	scanf(" %c", &answr);
	setPermiso(&mask_owner,permiso,(answr=='s') ? SET : UNSET);
	
	printf("Permisos para group\n");
	permiso = 'r';
	printf("Permiso lectura para group?[s/n]: ");
	scanf(" %c", &answr);
	setPermiso(&mask_group,permiso,(answr=='s') ? SET : UNSET);

	permiso = 'w';
	printf("Permiso escritura para group?[s/n]: ");
	scanf(" %c", &answr);
	setPermiso(&mask_group,permiso,(answr=='s') ? SET : UNSET);

	permiso = 'x';
	printf("Permiso ejecucion para group?[s/n]: ");
	scanf(" %c", &answr);
	setPermiso(&mask_group,permiso,(answr=='s') ? SET : UNSET);

	printf("Permisos para other\n");
	permiso = 'r';
	printf("Permiso lectura para other?[s/n]: ");
	scanf(" %c", &answr);
	setPermiso(&mask_other,permiso,(answr=='s') ? SET : UNSET);

	permiso = 'w';
	printf("Permiso escritura para other?[s/n]: ");
	scanf(" %c", &answr);
	setPermiso(&mask_other,permiso,(answr=='s') ? SET : UNSET);

	permiso = 'x';
	printf("Permiso ejecucion para other?[s/n]: ");
	scanf(" %c", &answr);
	setPermiso(&mask_other,permiso,(answr=='s') ? SET : UNSET);
	
	printf("\nMascara de permiso: %o%o%o",mask_owner,mask_group,mask_other);
}
